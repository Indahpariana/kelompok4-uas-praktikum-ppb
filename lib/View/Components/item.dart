import 'package:flutter/material.dart';
import 'package:weather/Model/weatherModel.dart';

class Item extends StatelessWidget {
  final WeeklyWeather item;
  final int day;

  const Item({super.key, required this.item, required this.day});

  @override
  Widget build(BuildContext context) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: myHeight * 0.015, horizontal: myWidth * 0.07),
      child: Container(
        height: myHeight * 0.11,
        decoration: BoxDecoration(
            color: Color.fromARGB(255, 21, 0, 73).withOpacity(0.05),
            borderRadius: BorderRadius.circular(24)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '$day June',
                  style: TextStyle(
                      color: Color.fromARGB(255, 15, 0, 99).withOpacity(0.5), fontSize: 17),
                ),
                Text(
                  item.day,
                  style: const TextStyle(color: Color.fromARGB(255, 15, 0, 84), fontSize: 20),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  item.mainTemp.replaceAll('°C', ''),
                  style: const TextStyle(color: Color.fromARGB(255, 40, 0, 96), fontSize: 55),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      '°C',
                      style: TextStyle(color: Color.fromARGB(255, 13, 0, 69), fontSize: 25),
                    ),
                    Text('')
                  ],
                ),
              ],
            ),
            if (item.mainImg.isNotEmpty)
              Image.asset(
                item.mainImg,
                height: myHeight * 0.05,
                width: myWidth * 0.1,
              )
            else
              SizedBox(
                height: myHeight * 0.05,
                width: myWidth * 0.1,
              ),
          ],
        ),
      ),
    );
  }
}