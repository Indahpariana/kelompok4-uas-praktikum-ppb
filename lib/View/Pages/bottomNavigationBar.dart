import 'package:flutter/material.dart';
import 'package:weather/Model/weatherModel.dart';
import 'package:weather/View/Pages/forecast.dart';
import 'package:weather/View/Pages/home.dart';
import 'package:weather/View/Pages/search.dart';
import 'package:weather/View/Pages/setting.dart';

class NavBar extends StatefulWidget {
  final List<WeatherModel> weatherModel;

  const NavBar({super.key, required this.weatherModel});

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  int _currentIndex = 0;

  late final List<Widget> pages;

  @override
  void initState() {
    pages = [
      Home(weatherModel: widget.weatherModel),
      Search(weatherModel: widget.weatherModel),
      Forecast(weatherModel: widget.weatherModel),
      const Setting(),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double myHeight = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromARGB(255, 203, 250, 255),
        body: pages[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Color.fromARGB(255, 41, 0, 106),
          currentIndex: _currentIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          onTap: (value) {
            setState(() {
              _currentIndex = value;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/icons/1.2.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255).withOpacity(0.5),
              ),
              label: '',
              activeIcon: Image.asset(
                'assets/icons/1.1.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/icons/2.2.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255).withOpacity(0.5),
              ),
              label: '',
              activeIcon: Image.asset(
                'assets/icons/2.1.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/icons/3.2.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255).withOpacity(0.5),
              ),
              label: '',
              activeIcon: Image.asset(
                'assets/icons/3.1.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/icons/4.2.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255).withOpacity(0.5),
              ),
              label: '',
              activeIcon: Image.asset(
                'assets/icons/4.1.png',
                height: myHeight * 0.03,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
          ],
        ),
      ),
    );
  }
}