import 'package:flutter/material.dart';

class Setting extends StatefulWidget {
  const Setting({super.key});

  @override
  State<Setting> createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  bool _darkMode = false;
  bool _autoUpdate = true;
  String _selectedLanguage = 'English';

  @override
  Widget build(BuildContext context) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;

    return MaterialApp(
      theme: _darkMode ? ThemeData.dark() : ThemeData.light(),
      home: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text('Pengaturan'),
          ),
          body: Container(
            height: myHeight,
            width: myWidth,
            padding: EdgeInsets.all(16.0),
            child: ListView(
              children: [
                ListTile(
                  title: Text('Tema Gelap'),
                  trailing: Switch(
                    value: _darkMode,
                    onChanged: (value) {
                      setState(() {
                        _darkMode = value;
                      });
                    },
                  ),
                ),
                Divider(),
                ListTile(
                  title: Text('Pembaruan Otomatis'),
                  trailing: Switch(
                    value: _autoUpdate,
                    onChanged: (value) {
                      setState(() {
                        _autoUpdate = value;
                      });
                    },
                  ),
                ),
                Divider(),
                ListTile(
                  title: Text('Bahasa'),
                  subtitle: Text(_selectedLanguage),
                  onTap: () async {
                    String? language = await showDialog<String>(
                      context: context,
                      builder: (BuildContext context) {
                        return LanguageDialog(selectedLanguage: _selectedLanguage);
                      },
                    );
                    if (language != null) {
                      setState(() {
                        _selectedLanguage = language;
                      });
                    }
                  },
                ),
                Divider(),
                ListTile(
                  title: Text('Tentang Aplikasi'),
                  onTap: () {
                    showAboutDialog(
                      context: context,
                      applicationName: 'Aplikasi Cuaca',
                      applicationVersion: '1.0.0',
                      applicationLegalese: '© 2024 Kelompok 4',
                      children: [
                        Text('Aplikasi Cuaca ini memberikan informasi cuaca terkini dan prakiraan cuaca harian.'),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LanguageDialog extends StatelessWidget {
  final String selectedLanguage;

  const LanguageDialog({required this.selectedLanguage});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Pilih Bahasa'),
      content: Container(
        width: double.minPositive,
        child: ListView(
          shrinkWrap: true,
          children: ['English', 'Indonesian', 'French', 'Spanish']
              .map(
                (language) => RadioListTile<String>(
                  title: Text(language),
                  value: language,
                  groupValue: selectedLanguage,
                  onChanged: (value) {
                    Navigator.pop(context, value);
                  },
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}