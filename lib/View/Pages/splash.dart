import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:weather/Model/weatherModel.dart';
import 'package:weather/View/Pages/bottomNavigationBar.dart';

class Splash extends StatefulWidget {
  const Splash({super.key});

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  List data = [];
  List<WeatherModel> weatherList = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await loadJson();
    });
  }

  Future<void> loadJson() async {
    String myData = await rootBundle.loadString('assets/myJson/file.json');

    setState(() {
      data = json.decode(myData);
      weatherList = data.map<WeatherModel>((e) => WeatherModel.fromJson(e)).toList();
      for (var weather in weatherList) {
        if (weather.name == 'Paris') {
          weather.name = 'Samarinda';
        }
      }
    });

    Timer(
      Duration(seconds: 2),
      () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => NavBar(weatherModel: weatherList),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(170, 255, 246, 1),
        body: Container(
          height: myHeight,
          width: myWidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(), // Placeholder for empty space
              Text(
                'APLIKASI CUACA',
                style: TextStyle(
                  color: Color.fromARGB(255, 13, 0, 109),
                  fontSize: 50,
                ),
              ),
              if (weatherList.isNotEmpty) ...[
                Image.asset(
                  weatherList[0].weeklyWeather[0].mainImg.toString(),
                  height: myHeight * 0.3,
                  width: myWidth * 0.8,
                ),
              ],
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'KELOMPOK 4',
                        style: TextStyle(
                          fontSize: 14,
                          color: Color.fromARGB(255, 41, 0, 111),
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      SizedBox(width: myWidth * 0.02),
                    ],
                  ),
                  SizedBox(height: myHeight * 0.009),
                  Image.asset(
                    'assets/loading1.gif',
                    height: myHeight * 0.015,
                    color: Color.fromARGB(255, 9, 0, 68),
                  ),
                  SizedBox(height: myHeight * 0.02),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}