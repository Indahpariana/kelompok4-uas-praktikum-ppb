import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../Model/weatherModel.dart';
import 'weather_detail_page.dart';

class Search extends StatefulWidget {
  List<WeatherModel> weatherModel = [];

  Search({required this.weatherModel});

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final TextEditingController _controller = TextEditingController();
  List<WeatherModel> _searchResults = [];

  Future<void> _searchWeather(String city) async {
    final apiKey = 'cbbd53bbec61afd2ed5c3568085a6e54';
    final url = 'https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apiKey&units=metric';

    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        _searchResults = [WeatherModel.fromJson(data)];
      });
    } else {
      // Handle error
      print('Error: ${response.reasonPhrase}');
    }
  }

  @override
  Widget build(BuildContext context) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(170, 255, 246, 1),
        body: Container(
          height: myHeight,
          width: myWidth,
          child: Column(
            children: [
              SizedBox(
                height: myHeight * 0.03,
              ),
              Text(
                'Pilih Lokasi',
                style: TextStyle(fontSize: 30, color: Color.fromARGB(255, 0, 14, 92)),
              ),
              SizedBox(
                height: myHeight * 0.03,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: myWidth * 0.05),
                child: Column(
                  children: [
                    Text(
                      'temukan daerah atau kota yang ingin anda ketahui',
                      style: TextStyle(fontSize: 18, color: Color.fromARGB(255, 41, 0, 98).withOpacity(0.5)),
                    ),
                    Text(
                      'info detail cuaca saat ini',
                      style: TextStyle(fontSize: 18, color: Color.fromARGB(255, 24, 0, 79).withOpacity(0.5)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: myHeight * 0.05,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: myWidth * 0.06),
                child: Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Color.fromARGB(255, 16, 0, 86).withOpacity(0.05),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: myWidth * 0.05),
                          child: TextFormField(
                            controller: _controller,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              icon: Icon(Icons.search, color: Color.fromARGB(255, 22, 0, 65).withOpacity(0.5)),
                              hintText: 'Mencari',
                              hintStyle: TextStyle(color: Color.fromARGB(255, 33, 0, 109).withOpacity(0.5)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: myWidth * 0.03,
                    ),
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () {
                          _searchWeather(_controller.text);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Color.fromARGB(255, 26, 0, 85).withOpacity(0.05),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: myHeight * 0.013),
                            child: Center(
                              child: Icon(Icons.search, color: Color.fromARGB(255, 14, 0, 95)),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: myHeight * 0.04,
              ),
              Expanded(
                child: GridView.custom(
                  padding: EdgeInsets.symmetric(horizontal: myWidth * 0.05),
                  gridDelegate: SliverStairedGridDelegate(
                    mainAxisSpacing: 13,
                    crossAxisSpacing: 10,
                    startCrossAxisDirectionReversed: true,
                    pattern: [
                      StairedGridTile(0.5, 1),
                      StairedGridTile(0.5, 3 / 4),
                    ],
                  ),
                  childrenDelegate: SliverChildBuilderDelegate(
                    (context, index) {
                      WeatherModel weather = _searchResults[index];
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => WeatherDetailPage(weather: weather),
                            ),
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: index.isEven ? Color.fromARGB(255, 17, 0, 69).withOpacity(0.1) : Color.fromARGB(255, 11, 0, 73).withOpacity(0.05),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: myHeight * 0.02, horizontal: myWidth * 0.05),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.cloud,
                                      color: Color.fromARGB(255, 14, 0, 83).withOpacity(0.7),
                                      size: 50,
                                    ),
                                  ],
                                ),
                                Text(
                                  weather.city,
                                  style: TextStyle(color: Color.fromARGB(255, 24, 0, 81), fontSize: 18),
                                ),
                                Text(
                                  '${weather.temp}°C',
                                  style: TextStyle(color: Color.fromARGB(255, 30, 0, 85).withOpacity(0.5), fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    childCount: _searchResults.length,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}