import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:weather/Model/weatherModel.dart';
import 'package:weather/Utils/staticFile.dart';


class Home extends StatefulWidget {
  List<WeatherModel> weatherModel = [];

  Home({required this.weatherModel});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await scrollToIndex();
    });
    find_myLocation_index();
    super.initState();
  }

  find_myLocation_index() {
    for (var i = 0; i < widget.weatherModel.length; i++) {
      if (widget.weatherModel[i].name == 'Samarinda') {
        setState(() {
          StaticFile.myLocationIndex = i;
          complete1 = true;
        });
        break;
      }
    }
    find_hour_index();
  }

  DateTime time = DateTime.now();
  int hour_index = 0;
  bool complete1 = false;
  bool complete2 = false;

  find_hour_index() {
    String my_time;
    my_time = time.hour.toString();
    if (my_time.length == 1) {
      my_time = '0' + my_time;
    }
    for (var i = 0;
        i <
            widget.weatherModel[StaticFile.myLocationIndex].weeklyWeather![0]!
                .allTime!.hour!.length;
        i++) {
      if (widget.weatherModel[StaticFile.myLocationIndex].weeklyWeather![0]!
              .allTime!.hour![i]!
              .substring(0, 2)
              .toString() ==
          my_time) {
        setState(() {
          hour_index = i;
          complete2 = true;
        });
        break;
      }
    }
  }

  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener =
      ItemPositionsListener.create();

  scrollToIndex() async {
    itemScrollController.scrollTo(
        index: hour_index,
        duration: Duration(seconds: 1),
        curve: Curves.easeInOutCubic);
  }

  @override
  Widget build(BuildContext context) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(170, 255, 246, 1),
        body: Container(
          height: myHeight,
          width: myWidth,
          child: Column(
            children: [
              SizedBox(
                height: myHeight * 0.03,
              ),
              Text(
                widget.weatherModel[StaticFile.myLocationIndex].name
                    .toString(),
                style: TextStyle(fontSize: 40, color: Color.fromARGB(255, 47, 0, 112)),
              ),
              SizedBox(
                height: myHeight * 0.01,
              ),
              Text(
                '28 Juni 2024'.toString(),
                style: TextStyle(
                    fontSize: 20, color: Color.fromARGB(255, 21, 0, 85).withOpacity(0.5)),
              ),
              SizedBox(
                height: myHeight * 0.05,
              ),
              Container(
                height: myHeight * 0.05,
                width: myWidth * 0.6,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 24, 0, 102).withOpacity(0.05),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 21, 85, 169),
                              Color.fromARGB(255, 139, 207, 255),
                            ],
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Perkiraan',
                            style: TextStyle(
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        child: Center(
                          child: Text(
                            'Kualitas Udara',
                            style: TextStyle(
                              color: Color.fromARGB(255, 21, 0, 92).withOpacity(0.5),
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: myHeight * 0.05,
              ),
              Image.asset(
                widget.weatherModel[StaticFile.myLocationIndex]
                    .weeklyWeather![0]!.mainImg
                    .toString(),
                height: myHeight * 0.3,
                width: myWidth * 0.8,
              ),
              SizedBox(
                height: myHeight * 0.05,
              ),
              SingleChildScrollView(
                child: Container(
                  height: myHeight * 0.06,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Column(
                          children: [
                            Text(
                              'Suhu',
                              style: TextStyle(
                                color: Color.fromARGB(255, 18, 0, 75).withOpacity(0.5),
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              widget.weatherModel[StaticFile.myLocationIndex]
                                  .weeklyWeather![0]!.mainTemp
                                  .toString(),
                              style: TextStyle(
                                color: Color.fromARGB(255, 3, 0, 82),
                                fontSize: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          children: [
                            Text(
                              'Angin',
                              style: TextStyle(
                                color: Color.fromARGB(255, 19, 0, 88).withOpacity(0.5),
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              widget.weatherModel[StaticFile.myLocationIndex]
                                  .weeklyWeather![0]!.mainWind
                                  .toString(),
                              style: TextStyle(
                                color: Color.fromARGB(255, 5, 0, 72),
                                fontSize: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          children: [
                            Text(
                              'Kelembaban',
                              style: TextStyle(
                                color: Color.fromARGB(255, 24, 0, 76).withOpacity(0.5),
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              widget.weatherModel[StaticFile.myLocationIndex]
                                  .weeklyWeather![0]!.mainHumidity
                                  .toString(),
                              style: TextStyle(
                                color: Color.fromARGB(255, 32, 0, 84),
                                fontSize: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: myHeight * 0.04,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: myWidth * 0.06),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Hari ini',
                      style: TextStyle(color: Color.fromARGB(255, 16, 0, 74), fontSize: 28),
                    ),
                    Text(
                      'Lihat Laporan Lengkap',
                      style: TextStyle(color: Color.fromARGB(255, 7, 0, 109), fontSize: 18),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: myHeight * 0.02,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(
                      left: myWidth * 0.03, bottom: myHeight * 0.03),
                  child: ScrollablePositionedList.builder(
                    itemScrollController: itemScrollController,
                    itemPositionsListener: itemPositionsListener,
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.weatherModel[StaticFile.myLocationIndex]
                        .weeklyWeather![0]!.allTime!.hour!.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: myWidth * 0.02,
                            vertical: myHeight * 0.01),
                        child: Container(
                          width: myWidth * 0.35,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: hour_index == index
                                ? null
                                : Colors.white.withOpacity(0.05),
                            gradient: hour_index == index
                                ? LinearGradient(
                                    colors: [
                                        Color.fromARGB(255, 21, 85, 169),
                                        Color.fromARGB(255, 44, 162, 246),
                                      ])
                                : null,
                          ),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  widget.weatherModel[StaticFile.myLocationIndex]
                                      .weeklyWeather![0]!
                                      .allTime!
                                      .img![index]
                                      .toString(),
                                  height: myHeight * 0.04,
                                ),
                                SizedBox(
                                  width: myWidth * 0.04,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      widget.weatherModel[StaticFile.myLocationIndex]
                                          .weeklyWeather![0]!
                                          .allTime!
                                          .hour![index]
                                          .toString(),
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Color.fromARGB(255, 19, 0, 89),
                                      ),
                                    ),
                                    Text(
                                      widget.weatherModel[StaticFile.myLocationIndex]
                                          .weeklyWeather![0]!
                                          .allTime!
                                          .temps![index]
                                          .toString(),
                                      style: TextStyle(
                                        fontSize: 25,
                                        color: Color.fromARGB(255, 28, 0, 93),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}