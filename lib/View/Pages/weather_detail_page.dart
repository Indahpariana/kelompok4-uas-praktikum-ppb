import 'package:flutter/material.dart';
import '../../Model/weatherModel.dart';

class WeatherDetailPage extends StatelessWidget {
  final WeatherModel weather;

  WeatherDetailPage({required this.weather});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(weather.city),
        backgroundColor: Color.fromRGBO(22, 24, 86, 1),
      ),
      backgroundColor: Color.fromRGBO(22, 24, 86, 1),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Cuaca Saat Ini',
              style: TextStyle(color: Colors.white, fontSize: 24),
            ),
            SizedBox(height: 10),
            Text(
              'Temperatur: ${weather.temp}°C',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            Text(
              'Kondisi: ${weather.condition}',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            Text(
              'Kecepatan Angin: ${weather.windSpeed} m/s',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            Text(
              'Kelembapan: ${weather.humidity}%',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            SizedBox(height: 20),
            Text(
              'Perkiraan Cuaca Mingguan',
              style: TextStyle(color: Colors.white, fontSize: 24),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: weather.weeklyWeather?.length ?? 0,
                itemBuilder: (context, index) {
                  final dailyWeather = weather.weeklyWeather![index];
                  return Card(
                    color: Colors.white.withOpacity(0.1),
                    child: ListTile(
                      title: Text(
                        dailyWeather.day ?? 'N/A',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Temperatur: ${dailyWeather.mainTemp}°C',
                            style: TextStyle(color: Colors.white.withOpacity(0.8)),
                          ),
                          Text(
                            'Kecepatan Angin: ${dailyWeather.mainWind} m/s',
                            style: TextStyle(color: Colors.white.withOpacity(0.8)),
                          ),
                          Text(
                            'Kelembapan: ${dailyWeather.mainHumidity}%',
                            style: TextStyle(color: Colors.white.withOpacity(0.8)),
                          ),
                          Text(
                            'Kondisi: ${dailyWeather.mainImg}',
                            style: TextStyle(color: Colors.white.withOpacity(0.8)),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
