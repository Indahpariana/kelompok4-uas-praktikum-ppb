class WeatherModel {
  WeatherModel({
    required this.name,
    required this.weeklyWeather,
  });

  String name;
  List<WeeklyWeather> weeklyWeather;

  factory WeatherModel.fromJson(Map<String, dynamic> json) => WeatherModel(
        name: json["name"] ?? '',
        weeklyWeather: json["weekly_weather"] == null
            ? []
            : List<WeeklyWeather>.from(json["weekly_weather"].map((x) => WeeklyWeather.fromJson(x))),
      );

  // Getter untuk mendapatkan nilai city
  String get city => name;

  // Getter untuk mendapatkan nilai temp
  String get temp => weeklyWeather.isNotEmpty
      ? weeklyWeather[0].mainTemp
      : '';

  // Getter untuk mendapatkan nilai condition
  String get condition => weeklyWeather.isNotEmpty
      ? weeklyWeather[0].mainImg
      : '';

  // Getter untuk mendapatkan nilai windSpeed
  String get windSpeed => weeklyWeather.isNotEmpty
      ? weeklyWeather[0].mainWind
      : '';

  // Getter untuk mendapatkan nilai humidity
  String get humidity => weeklyWeather.isNotEmpty
      ? weeklyWeather[0].mainHumidity
      : '';
}

class WeeklyWeather {
  WeeklyWeather({
    required this.mainTemp,
    required this.mainWind,
    required this.mainHumidity,
    required this.mainImg,
    required this.day,
    required this.allTime,
  });

  String mainTemp;
  String mainWind;
  String mainHumidity;
  String mainImg;
  String day;
  AllTime allTime;

  factory WeeklyWeather.fromJson(Map<String, dynamic> json) => WeeklyWeather(
        mainTemp: json["main_temp"] ?? '',
        mainWind: json["main_wind"] ?? '',
        mainHumidity: json["main_humidity"] ?? '',
        mainImg: json["main_img"] ?? '',
        day: json["day"] ?? '',
        allTime: json["all_time"] != null ? AllTime.fromJson(json["all_time"]) : AllTime.empty(),
      );
}

class AllTime {
  AllTime({
    required this.hour,
    required this.wind,
    required this.img,
    required this.temps,
  });

  List<String> hour;
  List<String> wind;
  List<String> img;
  List<String> temps;

  factory AllTime.fromJson(Map<String, dynamic> json) => AllTime(
        hour: json["hour"] == null ? [] : List<String>.from(json["hour"].map((x) => x)),
        wind: json["wind"] == null ? [] : List<String>.from(json["wind"].map((x) => x)),
        img: json["img"] == null ? [] : List<String>.from(json["img"].map((x) => x)),
        temps: json["temps"] == null ? [] : List<String>.from(json["temps"].map((x) => x)),
      );

  // Factory constructor to create an empty AllTime instance
  factory AllTime.empty() => AllTime(
        hour: [],
        wind: [],
        img: [],
        temps: [],
      );
}
